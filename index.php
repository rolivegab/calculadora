<!DOCTYPE html>
<html>
<head>
    <title>Calculadora</title>
    <link class="base" rel='shortcut icon' type='image/x-icon' href='favicon.ico' />
    <script class="base" type="text/javascript" src="client/SM.js"></script>
    <script class="base" type="text/javascript" src="client/App.js"></script>
    <script class="base">
        var app = new App();
        app.initialize();
    </script>
</head>
<body>
    <div id="content">
        
    </div>
</body>
</html>