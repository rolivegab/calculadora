<?php
    require_once( realpath(__DIR__.'/../../server/core/Page.php'));

    class Login extends Page
    {
        function __construct()
        {
            $resources = [];
            parent::__construct(__DIR__, $resources, self::class);
        }

        public function load()
        {
            M::addStyles($this->getStyles());
            M::addScripts($this->getScripts());
            M::set('content', $this->getContent());
        }
    }