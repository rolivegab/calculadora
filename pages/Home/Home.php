<?php
    require_once( realpath(__DIR__.'/../../server/core/Page.php'));
    
    class Home extends Page
    {
        function __construct()
        {
            $resources = [];
            parent::__construct(__DIR__, $resources, self::class);
        }

        public function load()
        {
            M::addStyles($this->getStyles());
            M::set('content', $this->getContent());
        }
    }