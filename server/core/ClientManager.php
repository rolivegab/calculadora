<?php
    require_once(__DIR__.'/Core.php');
    require_once(__DIR__.'/PageManager.php');
    // Para cada vez que acessar uma página: 
    // 1) Verifica se está logado. Se não estiver, joga para a página de Login.
    // 2) Verifica se existe um estado. Se não houver, joga para o HomeContent.

    if(PageManager::initialize())
    {
        PageManager::loadStatePage();
        PageManager::show();
    }