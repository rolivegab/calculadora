<?php

    abstract class Page
    {
        function __construct($dir, $resources, $name)
        {
            $this->dir = $dir;
            $this->resources = $resources;
            $this->name = $name;

            $this->cleanVariables();
            $this->loadResources();
            $this->loadStyles();
            $this->loadContent();
            $this->loadImages();
            $this->loadScripts();
        }

        private function cleanVariables()
        {
            $this->styles = [];
            $this->images = [];
            $this->scripts = [];
        }

        abstract function load();

        public function includePage(Page $page, string $id)
        {
            M::addInclude($id, $page->getContent());
            M::addStyles($page->getStyles());
            M::set('images', $page->getImages());
            M::addScripts($page->getScripts());
            $page->load();
        }

        public function getContent()
        {
            return $this->content;
        }

        public function getName()
        {
            return $this->name;
        }

        public function getStyles()
        {
            return $this->styles;
        }

        public function getResources()
        {
            return $this->resources;
        }

        public function getFonts()
        {
            return $this->fonts;
        }

        public function getImages()
        {
            return $this->images;
        }

        public function getScripts()
        {
            return $this->scripts;
        }

        private function loadResources()
        {
            // Iterating through resource categories
            $resources = $this->resources;
            foreach($resources as $key=>$c)
            {
                // Iterating throught resource category files
                foreach($c as $f)
                {
                    $dir = realpath(__DIR__.'/../resources/'.$key);
                    if($key == 'css')
                    {
                        $style = file_get_contents($dir.$f);
                        if($style)
                        {
                            $this->styles[$f] = $style;
                        }
                        else
                        {
                            M::setMessage('Erro ao carregar resource "'.$dir.$f.'".', M::ERROR);
                        }
                    }
                    else if($key == 'js')
                    {
                        if(isset($f['src']))
                        {
                            $this->scripts[] = ['level' => $f['level'], 'path' => $f['src']];
                        }
                        else if(isset($f['file']))
                        {
                            $uri_dir = self::$uri_base_dir.'server/resources/js';
                            $script = $dir.$f['file'];
                            if(is_file($script))
                            {
                                $this->scripts[$f['file']] = ['level' => $f['level'], 'path' => $uri_dir.$f['file']];
                            }
                            else
                            {
                                M::sendMessage('Erro ao carregar resource "'.$dir.$f['file'].'".', M::ERROR);
                            }
                        }
                    }
                }
            }
        }

        private function loadStyles()
        {
            if(!is_dir($this->dir.'/css'))
            {
                return;
            }

            $filenames = scandir($this->dir.'/css');
            $size = count($filenames);

            for($i = 2; $i < $size; $i++)
            {
                $this->styles[$filenames[$i]] = file_get_contents($this->dir.'/css/'.$filenames[$i]);
            }
        }

        private function loadContent()
        {
            $aux = $this->dir.'/content.html';
            if(is_file($aux))
            {
                $this->content = file_get_contents($aux);
            }
        }

        private function loadImages()
        {
            // Image directory)
            $dir = $this->dir.'/image';
            $uri_dir = self::$uri_base_dir.'pages/'.$this->name.'/image/';
            if(!is_dir($dir))
            {
                return;
            }

            $files = array_slice(scandir($dir), 2);

            foreach($files as $f)
            {
                $this->images['#'.$f] = $uri_dir.$f;
            }
        }

        private function loadScripts()
        {
            // Scripts directory:
            $dir = $this->dir.'/js';
            if(!is_dir($dir))
            {
                return;
            }

            $levels = array_slice(scandir($dir), 2);
            foreach($levels as $level)
            {
                $uri_dir = self::$uri_base_dir.'pages/'.$this->name.'/js/'.$level;
                $leveldir = $dir.'/'.$level;
                $files = array_slice(scandir($leveldir), 2);
                foreach($files as $f)
                {
                    $this->scripts[$f] = ['level' => (int)$level, 'path' => $uri_dir.'/'.$f];
                }
            }
        }

        protected function loadSQL($name, array $args = NULL)
        {
            //var_dump($args);
            $db = Core::ConnDB();
            $db->prepare(file_get_contents($this->dir.'/sql/'.$name));
            if($args)
            {
                foreach($args as $key=>$arg)
                {
                    $db->bindParam($key, $arg);
                }
            }
            $db->execute();
            return $db->fetchAll();
        }

        static private $uri_base_dir = '/calculadora/';

        private $name;
        private $dir;

        private $resources;
        private $styles;
        private $content;
        private $fonts;
        private $scripts;
        private $images;
    }