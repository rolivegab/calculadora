<?php
    abstract class M
    {
        public static function set($var, $value) : void
        {
            static::$message[$var] = $value;
        }

        public static function setArray(array $params)
        {
            $m = &static::$message;
            static::setArrayAux($m, $params);
        }

        private static function setArrayAux(&$m, $params)
        {
            $keys = array_keys($params);
            foreach($keys as $key)
            {
                if(is_array($params))
                {
                    if(isset($m[$key]))
                    {
                        static::setArrayAux($m[$key], $params[$key]);
                    }
                    else
                    {
                        $m[$key] = $params[$key];
                    }
                }
                else
                {
                    $m[$key] = $params[$key];
                }
            }
        }

        public static function send() : void
        {
            echo json_encode(static::$message);
        }

        public static function sendMessage($value, $const)
        {
            if($const == M::ERROR)
            {
                M::set('message', $value);
                M::set('messageType', 'ERROR');
            }
            else if($const == M::SUCCESS)
            {
                M::set('message', $value);
                M::set('messageType', 'SUCCESS');
            }
            else if($const == M::WARNING)
            {
                M::set('message', $value);
                M::set('messageType', 'WARNING');
            }
            else if($const == M::INFO)
            {
                M::set('message', $value);
                M::set('messageType', 'INFO');
            }
        }

        public static function addStyles(array $styles)
        {
            if(!isset(static::$message['styles']))
            {
                static::$message['styles'] = [];
            }

            foreach($styles as $key=>$style)
            {
                static::$message['styles'][$key] = $style;
            }
        }

        public static function addScripts(array $scripts)
        {
            if(!isset(static::$message['scripts']))
            {
                static::$message['scripts'] = [];
            }

            static::$message['scripts'] = array_merge(static::$message['scripts'], $scripts);
        }

        public static function addInclude(string $id, string $content)
        {
            if(!isset(static::$message['includes']))
            {
                static::$message['includes'] = [];
            }

            static::$message['includes'][$id] = $content;
        }

        public static $message;
        public const ERROR = 1;
        public const SUCCESS = 2;
        public const WARNING = 3;
        public const INFO = 4;
    }