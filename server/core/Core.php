<?php
    require_once(__DIR__.'/SM.php');
    /**
     * Classe principal do sistema.
     * @author Bielcito
     */
    abstract class Core
    {
        /**
        * Retorna o objeto responsável pelo gerenciamento do banco de dados.
        */
        public static function ConnDB()
        {
            if (static::$connDB) {
                return static::$connDB;
            } else {
                require_once( realpath(__DIR__.'/ConnDB.php') );
                static::$connDB = new ConnDB(new MoodleCredentials);
                return static::$connDB;
            }
        }

        /**
        * Retorna o objeto responsável pelo gerenciamento do sistema de login.
        */
        public static function LoginManager() : LoginManager
        {
            if (static::$loginManager) {
                return static::$loginManager;
            } else {
                require_once( realpath(__DIR__.'/LoginManager.php') );
                static::$loginManager = new loginManager(static::ConnDB());
                return static::$loginManager;
            }
        }

        /**
        * Enviar isso para uma outra classe e pensar em um padrão de projeto.
        */
        public function checkPOST() : void
        {
            if (!$this->LoginManager()->checkSession()) 
            {
                return;
            }

            // Se for taxa de Evasão:
            if (SM::isPOST('option') && SM::getPOST('option') == 1) 
            {
                require_once( realpath(__DIR__.'/../../pages/EvasionStat/EvasionStat.php') );
                $stat = new EvasionStat();
                $stat->checkPOST();
            }

            // Se for Módulos:
            if(SM::isPOST('option') && SM::getPOST('option') == 2)
            {
                require_once( realpath(__DIR__.'/../../pages/ModuleStat/ModuleStat.php') );
                $stat = new ModuleStat();
                $stat->checkPOST();
            }

            if(SM::isPOST('option') && SM::getPOST('option') == 3)
            {
                require_once( realpath(__DIR__.'/../../pages/ActivityStat/ActivityStat.php') );
                $stat = new ActivityStat();
                $stat->checkPOST();
            }

            // Se for da página Home:
            if (SM::isPOST('home')) 
            {
                require_once( realpath(__DIR__.'/../../pages/HomeContent/HomeContent.php') );
                $home = new HomeContent();
                $home->checkPOST();
            }

            // Se for da operação Create User:
            if(SM::isPOST('createuser'))
            {
                require_once(realpath(__DIR__.'/../../pages/CreateUser/CreateUser.php'));
                $createUser = new CreateUser();
                $createUser->checkPOST();
            }
        }

        public function PageManager() : PageManager
        {
            if ($this->pageManager)
            {
                return $this->pageManager;
            }
            else
            {
                require_once( realpath(__DIR__.'/PageManager.php') );
                $this->pageManager = new PageManager($this->LoginManager());
                return $this->pageManager;
            }
        }

        /**
		 * Imprime no console do navegador, aceita vários parâmetros.
		 */
		public static function echoConsole() : void
		{
			$args = func_get_args();
			echo '<script>console.log(';
			foreach($args as $key=>$arg)
			{
				$aux = json_encode($arg);
				if(json_last_error() !== JSON_ERROR_NONE)
				{
					echo json_encode('JSON Err: '.json_last_error_msg().'. at: '.__FILE__.' '.__LINE__);
				}

				if($key == 0)
				{
					echo $aux;
				}
				else
				{
					echo ', '.json_encode($arg);
				}
			}
			echo ');</script>';
		}

        private static $connDB;
        private static $loginManager;
        private $pageManager;
    }
