<?php

require_once( realpath(__DIR__.'/SM.php') );

class LoginManager
{
    public function __construct($dbconn)
    {
        $this->dbconn = $dbconn;
    }
    
    public function verifyPostVariables()
    {
        if (SM::isChanged('username') && SM::isChanged('password')) {
            return true;
        } else {
            return false;
        }
    }

    public function login($username, $password) : bool
    {
        if ($this->dbconn->checkLogin($username, $password))
        {
            if ($username != 'ead' && $username != 'admin' && $username != 'gabrielrocha' && $username != 'avany' && $username != 'vanessa' && $username != 'gracemoura' && $username != 'cst@unifacex.edu.br' && $username != 'administracao@unifacex.edu.br' && $username != 'prof.igorxavier@gmail.com' && $username != 'danbrasil1@gmail.com' && $username != 'divasueli@yahoo.com.br' && $username != 'isabella.lira@unifacex.edu.br' && $username != 'cpe@unifacex.edu.br' && $username != 'ivertonufrn@yahoo.com.br' && $username != 'nivialopes@yahoo.com.br' && $username != 'vaniaalberton@yahoo.com.br')
            {
                M::sendMessage('O usuário "'.$username.'" não possui permissão para acessar a Dashboard.', M::ERROR);
                return false;
            }

            SM::setSession('username', $username);
            SM::setSession('password', $password);

            return true;
        }
        else
        {
            M::sendMessage('Senha inválida.', M::ERROR);
        }
        
        return false;
    }

    // Checa se o usuário está logado através das variáveis de username e password armazenados na sessão.
    public function checkSession() : bool
    {
        if (SM::isSession('username') && SM::isSession('password'))
        {
            $username = SM::getSession('username');
            $password = SM::getSession('password');
            
            return $this->dbconn->checkLogin($username, $password);
        }
        
        return false;
    }
    
    public function logout()
    {
        SM::unsetSession('username');
        SM::unsetSession('password');
    }
    
    private $dbconn;
    private $enabledusers;
}
