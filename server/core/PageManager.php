<?php
    require_once(__DIR__.'/Page.php');
    require_once(__DIR__.'/M.php');
    require_once(__DIR__.'/SM.php');
    require_once(__DIR__.'/Core.php');

    /**
     * Gerencia as páginas, e o estado atual do sistema.
     */
    abstract class PageManager extends M
    {
        public static function initialize()
        {
            static::$pages = [];

            // Carrega as páginas
            if(static::loadPages())
            {
                // Caso não haja state padrão, joga para o Base:
                static::setFirstState();

                return true;
            }
            else
            {
                return false;
            }
        }

        private static function loadPages() : bool
        {
            // Diretório das páginas
            $pagesDir = realpath(__DIR__.'/../../pages');
            if(!$pagesDir)
            {
                echo 'Diretório de páginas não existente!';
                return false;
            }

            // Nome de cada página
            $pages = array_slice(scandir($pagesDir), 2);

            // Carrega cada página e a adiciona no gerenciador
            foreach($pages as $page)
            {
                $path = $pagesDir.'/'.$page;
                $class = $path.'/'.$page.'.php';
                if(is_dir($path) && is_file($class))
                {
                    include($class);
                    static::addPage(new $page());
                }
            }

            return true;
        }

        public static function setState(string $page)
        {
            M::set('state', $page);
            SM::setSESSION('state', $page);
        }

        public static function addPage(Page $page)
        {
            static::$pages[$page->getName()] = $page;
        }

        public static function loadStatePage()
        {
            // Verifica se está logado, caso não esteja, define state para Login
            static::checkLogin();

            // Executa o checkPOST da página referente ao campo 'ajax'
            // Altera o state para a página referente ao campo 'state'
            if(static::checkAjax() || static::checkState())
            {
                return;
            }
            
            M::set('reload', 'true');
            $page = static::$pages[SM::getSESSION('state')];
            $page->load();
        }

        public static function checkAjax()
        {
            if(!SM::isPOST('ajax'))
            {
                return false;
            }

            static::$pages[SM::getPOST('ajax')]->checkPOST();
            
            return true;
        }

        public static function checkState()
        {
            if(!SM::isPOST('state'))
            {
                return false;
            }

            static::setState(SM::getPOST('state'));

            return true;
        }

        public static function checkLogin()
        {
            // Caso esteja deslogado, ele seta o state para Login:
            $lm = Core::LoginManager();
            if(!$lm->checkSession())
            {
                static::setState('Login');
            }
            else
            {
                static::setState(SM::getSESSION('state'));
            }
        }

        public static function setFirstState()
        {
            if(!SM::isSESSION('state'))
            {
                static::setState('Base');
            }
        }

        public static function show()
        {
            M::send();
        }

        public static function getPage(string $page)
        {
            return static::$pages[$page];
        }

        private static $pages;
    }