<?php
    require_once( realpath(__DIR__.'/../imp/MoodleCredentials.php') );

    class ConnDB
    {
        public function __construct($Credentials)
        {
            // Permite a utilização das credenciais nesta classe.
            $this->dbname = $Credentials->DB()->dbname;
            $this->dbuser = $Credentials->DB()->dbuser;
            $this->dbpassword = $Credentials->DB()->dbpassword;
            $this->dbhost = $Credentials->DB()->dbhost;
            $this->isInTransaction = false;
            $this->Connect();
        }

        public function Connect()
        {
            $opt = array(
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            );

            try
            {
                $this->dbconn = new PDO("pgsql:host=$this->dbhost;dbname=$this->dbname", $this->dbuser, $this->dbpassword, $opt);
            }
            catch(Exception $e)
            {
                echo $e->getMessage();
            }
        }

        public function checkLogin($username, $password)
        {
            $sql = "SELECT username, password FROM mdl_user WHERE username = :username";
            $this->prepare($sql);
            $this->bindParam(':username', $username);
            $this->execute();
            if ($this->rowCount() == 0) {
                return false;
            } else {
                $row = $this->fetch();

                if(!function_exists('password_verify'))
                {
                    function password_verify($password, $hash)
                    {
                        return (crypt($password, $hash) === $hash);
                    }
                }
                if (password_verify($password, $row['password'])) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        public function bindParam($varname, $var)
        {
            $this->result->bindParam($varname, $var);
        }

        public function query($query)
        {
            return $this->dbconn->query($query);
        }

        public function prepare($query)
        {
            $this->result = $this->dbconn->prepare($query);
        }

        public function execute()
        {
            return $this->result->execute();
        }

        public function rowCount()
        {
            return $this->result->rowCount();
        }

        public function fetchAll()
        {
            return $this->result->fetchAll();
        }

        public function fetch()
        {
            return $this->result->fetch();
        }

		public function debugDumpParams()
        {
            return $this->result->debugDumpParams();
        }

        private $dbconn;
        private $result;

        private $dbname;
        private $dbuser;
        private $dbpassword;
        private $dbhost;
        private $isInTransaction;
    }
