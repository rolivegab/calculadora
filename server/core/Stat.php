<?php
    require_once ( realpath(__DIR__.'/../imp/MoodleCredentials.php') );
    require_once ( realpath(__DIR__.'/ConnDB.php') );

    abstract class Stat extends ConnDB
    {
        public function __construct()
        {
            parent::__construct(new MoodleCredentials());
            $this->message = [];
        }

        public function openFileTo($path)
        {
            ob_start();
            include($path);
            $contents = ob_get_contents();
            ob_end_clean();

            return $contents;
        }

        private $message;
    }