<?php

class _MailCredentials
{
    public function set($mail, $password, $host, $port, $security)
    {
        self::$mail = $mail;
        self::$password = $password;
        self::$host = $host;
        self::$port = $port;
        self::$security = $security;
    }

    // Fazer dps:
    public static function mail()
    {
        return(object)array(
                'mail' => self::$mail,
                'password' => self::$password,
                'host' => self::$host,
                'port' => self::$port,
                'security' => self::$security
        );
    }
    
    public static $mail;
    public static $password;
    public static $host;
    public static $port;
    public static $security;
}
