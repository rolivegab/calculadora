function reload(currentScript)
{
	window.dropdown = new Dropdown();
	currentScript.callback();
}

var currentScript = document.currentScript;
app.addFuncToReload(reload.bind(null, currentScript), currentScript);

function Dropdown()
{
	/**
	* Diz se um botão do dropdown está atualmente ativo ou inativo.
	*/
	this.open = function (button, id)
	{
		button.classList.toggle("active");
		document.getElementById("myDropdown" + id).classList.toggle("active");
	}.bind(this);

	this.changeplushint = function(i)
	{
		var plushint = document.getElementById('plus' + i);
		if(!plushint.classList.contains('opened'))
		{
			plushint.innerHTML = '-';
			plushint.classList.toggle('opened');
		}
		else
		{
			plushint.innerHTML = '+';
			plushint.classList.toggle('opened');
		}
	}.bind(this);

	/**
	* Função utilizada para facilitar na hora de sequenciar variáveis a serem enviadas por POST.
	*/
	this.g = function(i)
	{
		if(i[0] == 0)
		{
			i[0]++;
			return '';
		}
		else
		{
			i[0]++;
			return '&';
		}
	}.bind(this);

	// Close the dropdown menu if the user clicks outside of it
	/*window.onclick = function(event)
	{
		if (!event.target.matches('.dropbtn'))
		{

			var dropdowns = document.getElementsByClassName("dropdown-content");
			var i;

			for (i = 0; i < dropdowns.length; i++)
			{
				var openDropdown = dropdowns[i];
				if (openDropdown.classList.contains('show'))
				{
					openDropdown.classList.remove('show');
				}
			}
		}
	}*/

	/**
	* Inclui um script ao código, por enquanto, está sendo utilizado para incluir o gráfico quando os seus
	* dados já estão em mãos.
	*/
	this.includeJs = function(jsFilePath)
	{
		var script = document.createElement("script");

		script.type = "text/javascript";
		script.src = jsFilePath;
		document.getElementById('scripts').appendChild(script);

		return script;
	}.bind(this);

	this.createScript = function(name)
	{
		var script = document.createElement("script");
		script.type = "text/javascript";
		script.id = name;

		return script;
	}.bind(this);

	this.dateParse = function(elem)
	{
		elem.value=elem.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'');
	}.bind(this);

	this.resetPage = function()
	{
		document.getElementById('scripts').innerHTML = "";
		document.getElementById('content').innerHTML = "";

	}.bind(this);
}
