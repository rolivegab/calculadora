function reload(currentScript)
{
    if(!window.google)
    {
        var script = document.createElement('script');
        script.src = 'https://www.gstatic.com/charts/loader.js';
        script.onload = currentScript.callback;
        document.head.appendChild(script);
    }
    else
    {
        currentScript.callback();
    }
}

var currentScript = document.currentScript;
app.addFuncToReload(reload.bind(null, currentScript), currentScript);