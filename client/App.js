function App()
{
	/**
	 * Construtor da classe.
	 */
	this.initialize = function()
	{
		this.request(null, null);
	}.bind(this);

	this.request = function(header, callback, type, file, sync)
	{
		if(!header)
		{
			header = 'default';
		}
		if(!type)
		{
			type = "POST";
		}
		if(!file)
		{
			file = this.rootdir+"core/ClientManager.php";
		}
		if(!sync)
		{
			sync = true;
		}

		var xmlhttp = new XMLHttpRequest();

		xmlhttp.onreadystatechange = function()
		{
			if (xmlhttp.readyState == XMLHttpRequest.DONE && xmlhttp.status == 200)
			{
				if(callback)
				{
					this.automatic(xmlhttp.responseText, callback);
				}
				else
				{
					this.automatic(xmlhttp.responseText);
				}
			}
			else
			{
				
			}
		}.bind(this);

		xmlhttp.open(type, file, sync);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send(header);
	}.bind(this);

	this.automatic = function(responseText, callback)
	{
		var arrayText = JSON.parse(responseText);
		this.arrayText = arrayText;

		var styles = arrayText['styles'];
		var content = arrayText['content'];
		var message = arrayText['message'];
		var images = arrayText['images'];
		var scripts = arrayText['scripts'];
		var reload = arrayText['reload'];
		var includes = arrayText['includes'];
		if(arrayText['basestate'])
		{
			this.baseState = arrayText['basestate'];
		}
		if(arrayText['state'])
		{
			this.state = arrayText['state'];
		}

		if(reload)
		{
			this.reload();
		}
		if(styles)
		{
			this.loadStyles(styles);
		}
		if(content)
		{
			document.getElementById('content').innerHTML = content;
		}
		if(message)
		{
			messageType = arrayText['messageType'];
			this.printMessage(message, messageType);
		}
		if(includes)
		{
			this.loadIncludes(includes);
		}
		if(images)
		{
			this.loadImages(images);
		}
		if(scripts)
		{
			this.loadScripts(scripts);
		}

		// Continue execution:
		if(callback)
		{
			callback(arrayText);
		}
	}.bind(this);

	this.reload = function()
	{
		// Cleaning scripts:
		/*var autos = document.getElementsByTagName('script');
		var size = autos.length;
		for(var i = 0; i < size; i++)
		{
			if(autos[i].classList)
			{
				var classList = autos[i].classList;
				if(!classList.contains('base'))
				{
					autos[i].parentNode.removeChild(autos[i]);
					size--;
					i--;
				}
			}
		}*/

		// Cleaning Styles:
		var autos = document.getElementsByTagName('style');
		var size = autos.length;
		for(var i = 0; i < size; i++)
		{
			if(autos[i].classList)
			{
				var classList = autos[i].classList;
				if(!classList.contains('base') && !classList.contains('instance'))
				{
					autos[i].parentNode.removeChild(autos[i]);
					size--;
					i--;
				}
			}
		}
	}.bind(this);

	// Adiciona a função na estrutura de funções que devem ser executadas a cada reload.
	this.addFuncToReload = function(func, script)
	{
		var num = script.level;

		if(!this.funcs)
		{
			this.funcs = [];
		}

		if(!this.funcs[num])
		{
			this.funcs[num] = [];
		}

		// Verifica se a função já existe na estrutura, se não existir, adiciona:
		var aux = this.funcs[num];
		var flag = false;
		var src = script.getAttribute('src');
		Object.keys(aux).map(function(i, index)
		{
			if(aux[i]['src'] == src)
			{
				flag = true;
			}
		}.bind(this));

		if(!flag)
		{
			this.funcs[num].push({
				'src': src,
				'content': func
			});
		}
	}.bind(this);

	this.reloadFuncs = function(num)
	{
		if(!this.funcs)
		{
			return;
		}
		var f = this.funcs[num];
		if(!f)
		{
			return;
		}
		Object.keys(f).map(function(i,index){
			f[i]['content']();
		});
	}.bind(this);

	/**
	 * Adiciona os scripts do arrayText no LEVEL, depois os carrega no HEAD.
	 */
	this.loadScripts = function(scripts)
	{
		// Adding scripts to Level:
		Object.keys(scripts).map(function(i, index)
		{
			this.addScriptToLevel(scripts[i]['level'], scripts[i]['path']);
		}.bind(this));

		// Add scripts to HEAD:
		Object.keys(this.level).map(function(i, index)
		{
			this.loadLevelScripts(index);
		}.bind(this));
	}.bind(this);

	this.addScriptToLevel = function(num, content)
	{
		if(!this.level)
		{
			this.level = [];
		}
		if(!this.level[num])
		{
			this.level[num] = [];
			this.level[num]['loadcount'] = 0;
			this.level[num]['scripts'] = [];
		}
		else
		{
			this.level[num]['loadcount'] = 0;
		}

		// Check if script already exists in document, if so, don't append it:
		var exists = false;
		var scripts = document.getElementsByTagName('script');
		var scriptsSize = scripts.length;
		for(var j = 0; j < scriptsSize; j++)
		{
			if(scripts[j].getAttribute('src') == content)
			{
				exists = true;
				break;
			}
		}
		if(!exists)
		{
			this.level[num]['scripts'].push({
				content: content, 
				loaded: false
			});
		}
	}.bind(this);

	this.loadLevelScripts = function(num)
	{
		// Levels:
		var l = this.level;

		// Getting only the key of the level which will be loaded:
		var keys = Object.keys(l).sort();
		var key = keys[num];

		// Adding scripts to head:
		var aux = l[key]['scripts'];
		var size = aux.length;
		for(var i = 0; i < size; i++)
		{
			var script = document.createElement('script');
			script.src = aux[i]['content'];
			script.callback = this.prepareLoadLevelScripts.bind(null, num, num+1);
			script.level = num;
			script.state = this.baseState;
			this.addNumScripts(script.getAttribute('src'));
			script.onload = this.reduceScript.bind(this, script.getAttribute('src'));
			document.head.appendChild(script);
		}
	}.bind(this);

	this.addNumScripts = function(src)
	{
		if(!this.numScripts)
		{
			this.numScripts = [];
		}

		this.numScripts[src] = false;
	}.bind(this);

	this.reduceScript = function(src)
	{
		this.numScripts[src] = true;

		var flag = true;

		Object.keys(this.numScripts).map(function(i, index){
			if(this.numScripts[i] == false)
			{
				flag = false;
			}
		}.bind(this));

		if(flag == true)
		{
			this.reloadFuncs(0);
		}
	}

	this.prepareLoadLevelScripts = function(actual, next)
	{
		this.addLoadCount(actual);
		var l = this.level[actual];
		if(l['loadcount'] == l['scripts'].length)
		{
			this.reloadFuncs(next);
		}
	}.bind(this);

	this.addLoadCount = function(num)
	{
		this.level[num]['loadcount']++;
	}.bind(this);

	this.loadIncludes = function(includes)
	{
		Object.keys(includes).map(function(i, index)
		{
			var id = i;
			document.getElementById(id).innerHTML = includes[i];
		});
	}.bind(this);

	this.loadImages = function(images)
	{
		var imgs = document.getElementsByTagName('img'); 

		for(var i = 0; i < imgs.length; i++)
		{
			if(imgs[i].src)
			{
				if(images[imgs[i].getAttribute('src')])
				{	
					imgs[i].src = images[imgs[i].getAttribute('src')];
				}
			}
		}
	}.bind(this);

	this.loadStyles = function(styles)
	{
		Object.keys(styles).map(function(i, index){
			var style = document.createElement('style');
			style.classList.toggle('automatic');
			style.type = 'text/css';
			
			if(style.styleSheet)
			{
				style.styleSheet.cssText = styles[i];
			}
			else
			{
				style.appendChild(document.createTextNode(styles[i]));
			}

			document.head.appendChild(style);
		});
	}.bind(this);

	this.loadFonts = function(fonts)
	{

	}.bind(this);

	this.printMessage = function(message, messageType)
	{
		var wrapper = document.createElement('div');
		var div = document.createElement('div');
		var num = 0;
		while(true)
		{
			if(!document.getElementById('message'+num))
			{
				break;
			}

			num++;
		}

		wrapper.style.width = '100%';
		wrapper.style.position = 'absolute';
		wrapper.style.textAlign = 'center';
		wrapper.id = 'message'+num;

		div.style.borderRadius = '5px';
		div.style.display = 'inline-block';
		div.style.padding = '10px';
		div.style.color = 'white';

		var symbol = document.createElement('div');
		symbol.classList.toggle('font-awesome');
		symbol.innerHTML = '&#xf05a;';
		symbol.style.display = 'inline-block';
		symbol.style.marginRight = '10px';
		symbol.style.fontSize = '20px';

		var exit = document.createElement('exit');
		exit.classList.toggle('font-awesome');
		exit.innerHTML = '&#xf00d;';
		exit.style.display = 'inline-block';
		exit.style.marginLeft = '20px';
		exit.style.fontSize = '20px';
		exit.style.cursor = 'pointer';
		exit.onclick = this.deleteMessage.bind(this, num);

		div.appendChild(symbol);
		div.appendChild(document.createTextNode(message));
		div.appendChild(exit)

		if(messageType == 'ERROR')
		{
			div.style.backgroundColor = '#cc3333';
			symbol.innerHTML = '&#xf057;';
		}
		else if(messageType == 'INFO')
		{
			div.style.backgroundColor = '#3366cc';
			symbol.innerHTML = '&#xf05a;';
		}
		else if(messageType == 'WARNING')
		{
			div.style.backgroundColor = '#ccb333';
		}
		else if(messageType == 'SUCCESS')
		{
			div.style.backgroundColor = '#33cc79';
			symbol.innerHTML = '&#xf071;';
		}

		wrapper.appendChild(div);
		document.body.appendChild(wrapper);
		wrapper.style.top = (num*wrapper.offsetHeight)+'px';
	}.bind(this);

	this.deleteMessage = function(num)
	{
		var id = 'message'+num;
		var element = document.getElementById(id);
		element.parentNode.removeChild(element);
		while(true)
		{
			num++;
			var bottomElement = document.getElementById('message'+num);
			if(bottomElement)
			{
				bottomElement.style.top = (bottomElement.offsetHeight*(num-1))+'px';
				bottomElement.id = 'message'+(num-1);
				bottomElement.childNodes[0].childNodes[2].onclick = this.deleteMessage.bind(this, num-1);
				
			}
			else
			{
				break;
			}
		}
	}.bind(this);

	this.closePage = function()
	{
		document.getElementById('content') = '';
		document.getElementById('scripts') = '';
		document.getElementById('style') = '';
	}.bind(this);

	this.logout = function()
	{
		this.request('ajax=Login&logout=true', null);
	}.bind(this);

	this.open = function(page)
	{
		if(page == 'evasionStat')
		{
			this.evasionStat.open();
			this.SM.setCookie('state', page);
		}
		else if(page == 'home')
		{
			this.homeContent.open();
			this.SM.setCookie('state', page);
		}
		else if(page == 'moduleStat')
		{
			this.moduleStat.open();
			this.SM.setCookie('state', page);
		}
		else if(page == 'createUser')
		{
			this.createUser.open();
			this.SM.setCookie('state', page);
		}
		else if(page == 'activityStat')
		{
			this.activityStat.open();
			this.SM.setCookie('state', page);
		}
		else
		{
			console.log('Erro ao abrir página: estado da página não conhecido.');
		}
	}.bind(this);

	this.SM = new SM();
	this.rootdir = "/calculadora/server/";
	this.stateParams = [];
}